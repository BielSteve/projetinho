#coding: utf-8
from flask import Blueprint, render_template, request, url_for, redirect
from mod_login.login import validaSessao

from mod_cliente.clienteBD import Clientes

cliente = Blueprint('cliente', __name__, url_prefix='/cliente', template_folder='templates')

#Listar Clientes
@cliente.route("/", methods=['GET','POST'])
@validaSessao
def listarC():
    user=Clientes()
    
    res = user.selectUserALL()
    
    return render_template('listarClientes.html', result=res, content_type='application/json')
    

#Tela de Cadastro
@cliente.route("/cad", methods=['GET','POST'])
@validaSessao
def cad():
    titulo = 'Cadastrar'
    return render_template('formClientes.html', titulo=titulo)



@cliente.route('/editId', methods=['POST'])
def editId():

    user=Clientes()
    titulo = 'Editar'
    
    #realiza a busca pelo usuario e armazena o resultado no objeto
    executou = user.selectUser( request.form['id_clientes'] )
    print(executou)
    
    return render_template('formClientes.html', user=user, titulo=titulo, content_type='application/json')

#Metodo para editar clientes no banco
@cliente.route("/edit", methods=['GET','POST'])
@validaSessao
def edit():
    user=Clientes()

    user.id_clientes = request.form['id_clientes']
    user.nome  = request.form['nome']
    user.endereco = request.form['endereco']
    user.numero = request.form['numero']
    user.observacao = request.form['observacao']
    user.cep = request.form['cep']
    user.bairro = request.form['bairro']
    user.cidade = request.form['cidade']
    user.estado = request.form['estado']
    user.telefone = request.form['telefone']
    user.email = request.form['email']
    user.login = request.form['login']
    user.senha = request.form['senha']
    user.grupo = request.form['grupo']

    if 'salvaEditaClienteDB' in request.form:
        user.updateUser()
    elif 'excluiClienteDB' in request.form:
        user.deleteUser()
    
    
    return redirect('/cliente')

#Metodo para adicionar clientes no banco
@cliente.route('/addUser', methods=['POST'])
def addUser():
    
    user=Clientes()

    
    #user.id_usuario = request.form['id_usuario']
    user.nome  = request.form['nome']
    user.endereco = request.form['endereco']
    user.numero = request.form['numero']
    user.observacao = request.form['observacao']
    user.cep = request.form['cep']
    user.bairro = request.form['bairro']
    user.cidade = request.form['cidade']
    user.estado = request.form['estado']
    user.telefone = request.form['telefone']
    user.email = request.form['email']
    user.login = request.form['login']
    user.senha = request.form['senha']
    user.grupo = request.form['grupo']




    executou = user.insertUser()
    print(executou)

    return redirect('/cliente')


