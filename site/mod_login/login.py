import os
from flask import current_app, Blueprint, render_template, request, url_for, redirect, flash, session
from functools import wraps
from mod_login.formularios import LoginForm

from BancoBD import Banco
from mod_cliente.clienteBD import Clientes






login = Blueprint('login', __name__, url_prefix='/', template_folder='templates')







@login.route("/", methods=['GET','POST'])
def inicio():
    # form= LoginForm(request.form)
    # if form.validate_on_submit():
    #     user = User.query.filter(User.registry == form.registry.data).first()
    #     if user:
    #         if (user.password, form.password.data):
    #             session.clear()
    #             session.permanent = True
    #             session['user_id'] = user.id
    #             session['usuario'] = form.registry.data
    #             session['senha'] = form.password.data
                
    #             return redirect(url_for('home.inicial', form=form))
    #         else:
    #             flash('Credenciais inválidas')

                    
        
    # return render_template('login.html', form=form)
    form= LoginForm(request.form)
    if form.validate_on_submit():
        _name = form.registry.data
        _pass = form.password.data

        ValidaUserBanco = Clientes()
        userBanco = ValidaUserBanco.validaUsuario(_name ,_pass )

        userNameDB=0
        passwordDB=0
        grupoDB=0
        nomeDB=0
     
        for row in userBanco:
            id = row[0]
            userNameDB=row[1]
            passwordDB=row[2]
            grupoDB=row[3]
            nomeDB=row[4]

       # verifica se foi informado usuario e senha
        if _name and _pass and request.method == 'POST' and _name == userNameDB and _pass == passwordDB:
        #limpa a sessão
            session.clear()
        #registra usuario na sessão, armazenando o login do usuário
            session['id_clientes'] = id
            session['usuario'] = _name
            session['nome'] = nomeDB
            session['user_role'] = grupoDB
        #abre a aplicação na tela home
            return redirect(url_for('home.inicial'))
        else:
            flash('Credenciais inválidas')
    
    return render_template('login.html',form=form)
            
        

            
        
@login.route("/logout", methods=['GET','POST'])
def logoff():
    
    session.clear()
    return redirect( url_for('login.inicio') )    


# valida se o usuário esta ativo na sessão
def validaSessao(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'usuario' not in session:
#descarta os dados copiados da função original e retorna a tela de login
            return redirect(url_for('login.inicio',falhaSessao=5))
        else:
#retorna os dados copiados da função original
            return f(*args, **kwargs)

#retorna o resultado do if acima
    return decorated_function

    