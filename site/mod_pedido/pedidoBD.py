from BancoBD import Banco

class Pedidos(object):
#ultima alteração aqui.........
    def __init__(self, id_pedidos=0, data_hora="", observacao=0, tb_clientes_id_clientes=0):
        self.info = {}
        self.id_pedidos = id_pedidos
        self.data_hora = data_hora
        self.observacao = observacao
        self.tb_clientes_id_clientes = tb_clientes_id_clientes
       


    def selectPedALL(self):
        banco=Banco()
        try:
            c=banco.conexao.cursor()
            c.execute("select id_pedidos, data_hora, observacao, tb_clientes_id_clientes from tb_pedidos")
            result = c.fetchall()
            c.close()
            return result
        except:
            return "Ocorreu um erro na busca do pedido"


    def selectPed(self, id_pedidos):
        banco=Banco()
        try:
            c=banco.conexao.cursor()
            c.execute("select id_pedidos, data_hora, observacao, tb_clientes_id_clientes from tb_pedidos where id_pedidos = %s" , (id_pedidos))
            
            for linha in c:
                
                self.id_pedidos = linha[0]
                self.data_hora = linha[1]
                self.observacao = linha[2]
                self.tb_clientes_id_clientes = linha[3]
                
            c.close()

            return "Busca feita com sucesso!"
        except:
            return "Ocorreu um erro na busca de um pedido"


    def insertPed(self):

        banco = Banco()
        try:
            
            c = banco.conexao.cursor()
            c.execute("insert into tb_pedidos(observacao, tb_clientes_id_clientes) values (%s, %s)" ,
            (self.observacao, self.tb_clientes_id_clientes))
            banco.conexao.commit()
            self.id_pedidos = c.lastrowid
            c.close()

            return "Pedido cadastrado com sucesso!"
        except:
            return "Ocorreu um erro na inserção do pedido"


    def updatePed(self):

        banco=Banco()
        try:

            c=banco.conexao.cursor()
            c.execute("update tb_pedidos set observacao = %s where id_pedidos=%s" , 
            (self.observacao, self.id_pedidos))
            banco.conexao.commit()
            c.close()

            return "Pedido atualizado com sucesso!"
        except:
            return "Ocorreu um erro na alteração do Produto"
    
    

    def delete(self, id):
        banco=Banco()
        try:
            c=banco.conexao.cursor()
            c.execute("delete from tb_pedido_produtos where tb_pedidos_id_pedidos = %s" , (id))
            c.execute("delete from tb_pedidos where id_pedidos = %s" , (id))
            banco.conexao.commit()
            c.close()
            return 'Pedido excluído com sucesso!'
        except:
            return 'Ocorreu um erro na exclusão do pedido'


    


    