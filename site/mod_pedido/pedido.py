#coding: utf-8
from flask import Blueprint, render_template, redirect, url_for, request, jsonify, session, flash, current_app, make_response
from mod_login.login import validaSessao
import base64
from base64 import b64encode
import pdfkit


from mod_cliente.clienteBD import Clientes
from mod_produto.produtoBD import Produtos
from mod_pedido.pedidoBD import Pedidos
from mod_pedido.pedidoProdBD import PedidosProd




pedido = Blueprint('pedido', __name__, url_prefix='/pedido', template_folder='templates')

@pedido.route("/", methods=['GET','POST'])
@validaSessao
def listarPedido():

    ped= Pedidos()
    res= ped.selectPedALL()
    user=Clientes()



    return render_template('listarPedidos.html', result=res, content_type='application/json')

@pedido.route("/cad")
@validaSessao
def cad():

    ped= Pedidos()
    prod = Produtos()
    user=Clientes()
    
    res = prod.selectProdALL()
    user = user.selectUserALL()
    titulo = 'Cadastrar'
    return render_template('formPedidos.html', titulo=titulo, result=res, ped=ped)

@pedido.route("/prodId/<int:id>")
@validaSessao
def prodId(id):
    prod = Produtos()
    produto = prod.selectProd(id)
    if produto == 'Busca feita com sucesso!':
        
        return jsonify({'id_produtos': str(prod.id_produtos), 'valor': str(prod.valor), 'imagem': prod.imagem})
    else:
        return jsonify({'mensagem': 'mensagem de erro aqui'})



@pedido.route('/editId/<int:id>', methods=['GET','POST'])
def editId(id):

    ped=Pedidos()
    pedProd=PedidosProd()
    prod = Produtos()

    titulo = 'Editar'
    produtos= pedProd.selectProdPedALL(id)
    res = prod.selectProdALL()


    
   #realiza a busca pelo produto e armazena o resultado no objeto
   
    executou = ped.selectPed(id)
    
    
        
    return render_template('formPedidos.html', ped=ped, Titulo=titulo, produtos=produtos,result=res, content_type='application/json')

#Metodo para editar produtos no banco
@pedido.route("/edit,<int:id>", methods=['GET','POST'])
@validaSessao
def edit(id):
    ped=Pedidos()
    if request.form :

                    
        if 'excluiPedidoDB' in request.form:
            
            ped.delete(id)
    
    
            return redirect('/pedido')

#Metodo para adicionar clientes no banco
@pedido.route('/addPed', methods=['POST' , 'GET'])
def addPed():
    
    if request.form['id_pedidos'] == '':
        flash('Você não pode salvar o pedido sem itens. Por favor Clique em Adicionar Produto')
        return redirect('/pedido/cad')

    ped=Pedidos()

 
    
    #user.id_usuario = request.form['id_produtos']
    ped.id_pedidos = request.form['id_pedidos']
    ped.observacao = request.form['observacao']
    
    


    executou = ped.updatePed()
    return redirect(url_for('pedido.editId', id= ped.id_pedidos))

@pedido.route('/salvaAjax', methods=['POST'])
def salvaAjax():
    
    if(request.form['observacao']):

        ped=Pedidos()

    
        #user.id_usuario = request.form['id_produtos']
        
        ped.observacao = request.form['observacao']
        ped.tb_clientes_id_clientes = session.get('id_clientes', None)
    
        executou = ped.insertPed()
        if executou == 'Pedido cadastrado com sucesso!':
            return jsonify({'id_pedidos': str(ped.id_pedidos)})
        return jsonify({'mensagem': executou})
    else:
        return jsonify({'mensagem': 'Parametros invalidos'})

@pedido.route('/salvaProdutoAjax', methods=['POST'])
def salvaProdutoAjax():
    if request.form['tb_pedidos_id_pedidos']:
        ped=PedidosProd()

        ped.quantidade = request.form['quantidade']
        ped.valor = request.form['valor']
        ped.observacao = request.form['observacao']
        ped.tb_pedidos_id_pedidos = request.form['tb_pedidos_id_pedidos']
        ped.tb_produtos_id_produtos = request.form['tb_produtos_id_produtos']
        
        executou = ped.insertProdPed()

        if executou == 'Pedido cadastrado com sucesso!':
            return jsonify({'id_pedidos': str(ped.tb_pedidos_id_pedidos)})
        return jsonify({'mensagem': executou})
    else:
        return jsonify({'mensagem': 'Parametros invalidos'})


# ajax
@pedido.route('/deleteProdPed', methods=['POST'])
def deleteProdPed():
    if (request.form['order_id'] and request.form['product_id']):
        ped = PedidosProd()
        ped.tb_pedidos_id_pedidos = request.form['order_id']
        ped.tb_produtos_id_produtos = request.form['product_id']
        executou = ped.deleteProdPed()


        return jsonify({'message': executou})
    return jsonify({'message': 'O parâmetro ID do pedido e ID do produto são obrigatórios'})



# ajax
@pedido.route('/editProdPed', methods=['POST'])
def editProdPed():
    if (request.form['order_id'] and request.form['product_id']):
        ped=PedidosProd()
        ped.tb_pedidos_id_pedidos = request.form['order_id']
        ped.tb_produtos_id_produtos = request.form['product_id']
        ped.quantidade = request.form['quantidade']
        ped.observacao = request.form['observacao']
        ped.valor = request.form['valor']
        executou = ped.updateProdPed()
        return jsonify({'message': executou})
    return jsonify({'message': 'O parâmetro ID do pedido e ID do produto são obrigatórios'})


@pedido.route('/report/<int:id>')
def report(id): 

    ped = Pedidos()
    ret = ped.selectPed(id)
    if not ped.id_pedidos:
        flash(ret, 'info')
        return redirect(url_for('pedido.listarPedido'))

    ped_p = PedidosProd()
    products = ped_p.selectProdPedALL(id)
 

    ren = render_template('pedido_pdf.html', products=products, ped=ped)
    pdf = pdfkit.from_string(ren, False)
    response = make_response(pdf)
    response.headers['Content-Type'] = 'application/pdf'
    response.headers['Content-Disposition'] = 'attachement; filename=relatorio-pedido.pdf'
    return response