from BancoBD import Banco

class PedidosProd(object):
#ultima alteração aqui.........
    def __init__(self, quantidade="", valor=0, observacao="", tb_pedidos_id_pedidos=0, tb_produtos_id_produtos=0):
        self.info = {}
        self.quantidade = quantidade
        self.valor = valor
        self.observacao = observacao
        self.tb_pedidos_id_pedidos = tb_pedidos_id_pedidos
        self.tb_produtos_id_produtos = tb_produtos_id_produtos

    
    
    def insertProdPed(self):

        banco = Banco()
        try:
            
            c = banco.conexao.cursor()
            c.execute("insert into tb_pedido_produtos(quantidade, valor, observacao, tb_pedidos_id_pedidos, tb_produtos_id_produtos) values (%s, %s, %s, %s, %s)" ,
            (self.quantidade, self.valor, self.observacao, self.tb_pedidos_id_pedidos, self.tb_produtos_id_produtos))
            banco.conexao.commit()
            c.close()

            return "Pedido cadastrado com sucesso!"
        except:
            return "Ocorreu um erro na inserção do pedido"

    
    def selectProdPed(self, tb_pedidos_id_pedidos):
        banco=Banco()
        try:
            c=banco.conexao.cursor()
            c.execute("select quantidade, valor, observacao, tb_pedidos_id_pedidos, tb_produtos_id_produtos from tb_pedido_produtos where tb_pedidos_id_pedidos = %s" , (tb_pedidos_id_pedidos))
            
            for linha in c:
                
                self.quantidade = linha[0]
                self.valor = linha[1]
                self.observacao = linha[2]
                self.tb_pedidos_id_pedidos = linha[3]
                self.tb_produtos_id_produtos = linha[4]

                
            c.close()

            return "Busca feita com sucesso!"
        except:
            return "Ocorreu um erro na busca de um produto"
    

    def deleteProdPed(self):

        banco=Banco()
        try:

            c=banco.conexao.cursor()
            c.execute("delete from tb_pedido_produtos where tb_produtos_id_produtos = %s and tb_pedidos_id_pedidos = %s" , (self.tb_produtos_id_produtos, self.tb_pedidos_id_pedidos))
            banco.conexao.commit()
            c.close()

            return "Produto excluído com sucesso!"
        except:
            return "Ocorreu um erro na exclusão do Produto"


    def selectProdPedALL(self):
        banco=Banco()
        try:
            c=banco.conexao.cursor()
            c.execute("select quantidade, valor, observacao, tb_pedidos_id_pedidos, tb_produtos_id_produtos from tb_pedidos_id_pedidos")
            result = c.fetchall()
            c.close()
            return result
        except:
            return "Ocorreu um erro na busca do pedido"
       
    
    def updateProdPed(self):

        banco=Banco()
        

        c=banco.conexao.cursor()
        c.execute("update tb_pedido_produtos set quantidade = %s, valor = %s, observacao = %s where tb_pedidos_id_pedidos = %s and tb_produtos_id_produtos = %s" , 
        (self.quantidade, self.valor, self.observacao, self.tb_pedidos_id_pedidos, self.tb_produtos_id_produtos))
        banco.conexao.commit()
        c.close()

        return "Produto atualizado com sucesso!"
        
            # return "Ocorreu um erro na alteração do Produto"


    def selectProdPedALL(self, id_pedido):
        banco=Banco()
        try:
            c = banco.conexao.cursor()
            c.execute('SELECT tb_pedido_produtos.quantidade, tb_pedido_produtos.valor, tb_pedido_produtos.observacao, tb_pedido_produtos.tb_pedidos_id_pedidos, tb_pedido_produtos.tb_produtos_id_produtos, tb_produtos.descricao, tb_produtos.valor, CONVERT(tb_produtos.imagem USING utf8) FROM tb_pedido_produtos LEFT JOIN tb_produtos ON tb_pedido_produtos.tb_produtos_id_produtos = tb_produtos.id_produtos WHERE tb_pedido_produtos.tb_pedidos_id_pedidos = %s' , (id_pedido))
            result = c.fetchall()
            c.close()
            return result
        except:
            return None
  