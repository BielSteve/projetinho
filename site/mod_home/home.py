#coding: utf-8
from flask import Blueprint, render_template
from mod_login.login import validaSessao


home = Blueprint('home', __name__, url_prefix='/home', template_folder='templates')

@home.route("/", methods=['GET','POST'])
@validaSessao
def inicial():
    return render_template('index.html')