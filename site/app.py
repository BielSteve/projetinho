#coding: utf-8
from flask import Flask, redirect, request, session
import os
from datetime import timedelta

from mod_home.home import home
from mod_cliente.cliente import cliente
from mod_produto.produto import produto
from mod_pedido.pedido import pedido
from mod_erro.erro import erro
from mod_login.login import login
    





def create_app():
    app = Flask(__name__)
   # app.config['SECRET_KEY']='JOAO'
    app.secret_key = os.urandom(12).hex()

    app.register_blueprint(home)
    app.register_blueprint(cliente)
    app.register_blueprint(produto)
    app.register_blueprint(pedido)
    app.register_blueprint(erro)
    app.register_blueprint(login)


    @app.before_request
    def before_request():
        session.permanent = True
        app.permanent_session_lifetime = timedelta(seconds=2000)

    @app.errorhandler(404)
    def nao_encontrado(error):
        return redirect("/erro/404",code=302)

    @app.errorhandler(500)
    def problema_servidor(error):
        return redirect("/erro/500",code=302), 500

    


    return app