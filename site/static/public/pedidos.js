//Iniciando JavaScript e Ajax, modal..... 
$('#descricao').on('change', function () {
    var prodid = $(this).val();
    if (prodid != '') {


        $.ajax({
            method: 'GET',
            url: '/pedido/prodId/' + prodid
        }).done(function (dados) {
            console.log(dados)
            if (dados.id_produtos) {
                $('#prod_img').attr('src', dados.imagem)
                $('#valor').val(dados.valor)
                $('#p_valor').val(dados.valor)
            } else {
                alert(dados.mensagem);
            }
        })
    }
});
//Metodo de Adiciona produto no pedido
function addPed(id_pedidos) {
    $.ajax({
        method: 'POST',
        url: '/pedido/salvaProdutoAjax',
        data: {
            quantidade: $('#quantidade').val(),
            valor: $('#valor').val(),
            observacao: $('#observacaoProd').val(),
            tb_pedidos_id_pedidos: $('#id_pedidos').val(),
            tb_produtos_id_produtos: $('#descricao').val()


        }
    }).done(function (dados) {
        console.log(dados)
        if (dados.id_pedidos) {
            $('#id_pedidos').val(dados.id_pedidos)
            addProductToFrontend()
            
        } else {
            alert(dados.mensagem)
        }
    })

}

//Salvar na tabela de baixo
$('#salvarTabela').on('click', function () {
    var idped = $('#id_pedidos').val()
    if (idped != '') {
        //Todo: Adicionar produto aqui
        addPed(idped)


    } else {

        if ($('#descricao').val() != '') {

            $.ajax({
                method: 'POST',
                url: '/pedido/salvaAjax',
                data: {
                    observacao: 'kdsfjdsf'
                }
            }).done(function (dados) {
                console.log(dados)
                if (dados.id_pedidos) {
                    $('#id_pedidos').val(dados.id_pedidos)
                    //Todo: Adicionar produto aqui
                    addPed(dados.id_pedidos)
                } else {
                    alert(dados.mensagem)
                }
            })
        } else {
            alert('Selecione um Produto')
        }
    }
});

// Preencher a tabela de baixo com os dados do model
function addProductToFrontend() {
    var row = $('<tr>');

    var td_img = $('<td><a href="' + $('#prod_img').attr('src') + '" data-lightbox="' + $('#descricao').val() + '" data-title="' + $('#descricao').val() + '"><img src="' + $('#prod_img').attr('src') + '" /></a></td>');
    row.append(td_img);

    var td_id = $('<td class="row-id">' + $('#descricao').val() + '</td>');
    row.append(td_id);

    var td_desc = $('<td>' + $('#descricao option:selected').html() + '</td>');
    row.append(td_desc);

    var td_val_u = $('<td class="row-value">' + $('#p_valor').val() + '</td>');
    row.append(td_val_u);

    var td_qtd = $('<td><input type="number" class="form-control edit-qtd-field qtd-field" value="' + $('#quantidade').val() + '" /></td>');
    row.append(td_qtd);

    var td_val = $('<td class="row-total">' + $('#valor').val() + '</td>');
    row.append(td_val);

    var td_obs = $('<td><input type="text" class="form-control obs-field" value="' + $('#observacaoProd').val() + '" /></td>');
    row.append(td_obs);

    var td_edit = $('<td><button type="button" class="btn btn-primary p-edit"><i class="fa fa-edit"></i>Editar</button> <button type="button" class="btn btn-danger p-delete"><i class="fa fa-trash-o"></i>Deletar</button></td>');
    row.append(td_edit);

    $('#p-body').append(row);
    $('#p-has').removeClass('none');
    $('#p-not-has').addClass('none');
    $('#exampleModal').modal('toggle');
}
//Função pra mudar a quantidade no campo da tabela
$('#quantidade').on('keyup, change ', function(){

    var quantidade = $(this).val();
    var valor = $('#p_valor').val();
    if (quantidade >= 1 && quantidade != ''){
        
        $('#valor').val(calculatotal(quantidade, valor));

    }
    else{
       $(this).val('1'); 
    }

})

function calculatotal(quantidade, valor){
    var total = quantidade * valor;
    total = total.toFixed(2);
    return total;
}

// Edição de dados frontend
$('body').on('input', '.edit-qtd-field', function() {
    var t = $(this);
    var newQtd = t.val();
    var row = t.parent().parent();
    var f_total = row.find('.row-total');
    var f_value = row.find('.row-value');
    var value = parseFloat(f_value.text()).toFixed(2);

    if (isNaN(newQtd) || newQtd < 1) {
        t.val('1');
        newQtd = 1;
    }

    var newTotal = value * newQtd;
    f_total.html(newTotal.toFixed(2));
});


 // Edição de dados
 $('body').on('click', '.p-edit', function() {
    var t = $(this);
    var row = t.parent().parent();
    var order_id = $('#id_pedidos').val();
    var product_id = row.find('.row-id').text();
    var qtd = row.find('.qtd-field').val();
    var obs = row.find('.obs-field').val();
    var total = row.find('.row-total').text();

    editProductFromOrder(order_id, product_id, qtd, obs, total);
});


 // Edição de dados
 function editProductFromOrder(order_id, product_id, qtd, obs, total) {
    $.ajax({
        method: 'POST',
        url: '/pedido/editProdPed',
        data: { order_id: order_id, product_id: product_id, quantidade: qtd, observacao: obs, valor: total }
    }).done(function (data) {
        if (data && data.message) {
            $('#textModalMessage').html(data.message);
        } else {
            $('#textModalMessage').html('Desculpe, ocorreu um problema ao editar este item.');
        }
        $('#modalMessage').modal('toggle');
    });
}

 // Deletar direto do banco pela tabela
 $('body').on('click', '.p-delete', function() {
    var t = $(this);
    var row = t.parent().parent();
    var order_id = $('#id_pedidos').val();
    var product_id = row.find('.row-id').text();

    if ($('#page-origin').val() === 'edit-page') {
        if ($('#p-body').find('.row-id').length <= 1) {
            $('#textModalMessage').html('O pedido não pode ter menos que um produto.');
            $('#modalMessage').modal('toggle');
            return false;
        }
    }

    deleteProductFromOrder(order_id, product_id, row);
});


// Deleta produto do pedido, direto do servidor
function deleteProductFromOrder(order_id, product_id, row) {
    $.ajax({
        method: 'POST',
        url: '/pedido/deleteProdPed',
        data: { order_id: order_id, product_id: product_id}
    }).done(function (data) {
        if (data && data.message) {
            $('#textModalMessage').html(data.message);
            deleteFrontendProductFromOrder(row);
        } else {
            $('#textModalMessage').html('Desculpe, ocorreu um problema ao deletar este item.');
        }
        $('#modalMessage').modal('toggle');
    });
}

// Deleta o produto do pedido frontend
function deleteFrontendProductFromOrder(row) {
    row.remove();
    if ($('#p-body').find('tr').length < 1) {
        $('#p-has').addClass('none');
        $('#p-not-has').removeClass('none');
        
    }
}
