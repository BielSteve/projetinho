#coding: utf-8
from flask import Blueprint, render_template, redirect, url_for, request
from mod_login.login import validaSessao
import base64

from mod_produto.produtoBD import Produtos
from mod_cliente.clienteBD import Clientes


produto = Blueprint('produto', __name__, url_prefix='/produto', template_folder='templates')

@produto.route("/", methods=['GET','POST'])
@validaSessao
def listarP():

    prod= Produtos()
    res= prod.selectProdALL()

    return render_template('listarProdutos.html', result=res, content_type='application/json')

@produto.route("/cad", methods=['GET','POST'])
@validaSessao
def cad():
    prod= Produtos()
    titulo = 'Cadastrar'

    return render_template('formProdutos.html', prod=prod, titulo=titulo)


@produto.route('/editId', methods=['POST'])
def editId():

    prod=Produtos()
    titulo = 'Editar'
    
   #realiza a busca pelo produto e armazena o resultado no objeto
    executou = prod.selectProd( request.form['id_produtos'] )
        
    return render_template('formProdutos.html', prod=prod, titulo=titulo, content_type='application/json')

#Metodo para editar produtos no banco
@produto.route("/edit", methods=['GET','POST'])
@validaSessao
def edit():
    prod=Produtos()
    if request.form :
    
        prod.id_produtos = request.form['id_produtos']  
        prod.descricao  = request.form['descricao']
        prod.valor = request.form['valor']
        
        #prod.imagem =  "data:" + request.files['imagem'].content_type + ";base64," + str(base64.b64encode( request.files['imagem'].read() ) , "utf-8")
        #prod.imagem = (base64.b64encode( request.files['imagem'].read()

        if request.files['imagem_edicao'].filename != '' :
            prod.imagem =  "data:" + request.files['imagem_edicao'].content_type + ";base64," + str(base64.b64encode( request.files['imagem_edicao'].read() ) , "utf-8")

        else:
            prod2=Produtos()
            executou = prod2.selectProd( request.form['id_produtos'] ) 
            prod.imagem  = prod2.imagem
            
        if 'salvaEditaProdutoDB' in request.form:
            prod.updateProdImg()
        elif 'excluiProdutoDB' in request.form:
            prod.deleteProd()
    
    
        return redirect('/produto')

#Metodo para adicionar clientes no banco
@produto.route('/addProd', methods=['POST'])
def addProd():
    
    prod=Produtos()

    
    #user.id_usuario = request.form['id_produtos']
    prod.id_produto = request.form['id_produtos']
    prod.descricao = request.form['descricao']
    prod.valor = request.form['valor']
    prod.imagem =  "data:" + request.files['imagem'].content_type + ";base64," + str(base64.b64encode( request.files['imagem'].read() ) , "utf-8")
    


    executou = prod.insertProd()
    

    return redirect('/produto')